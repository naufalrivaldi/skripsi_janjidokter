<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('Pasien_model', 'pasien');
    $this->load->model('Dokter_model', 'dokter');
    $this->load->model('Jadwal_model', 'jadwal');
  }

	public function index(){
    if(!empty($this->jadwal->nomerAntrian())){
      $nomer = $this->jadwal->nomerAntrian();
      $data['nomer'] = $nomer->nomer;
    }else{
      $data['nomer'] = '-';
    }

		$this->load->view('login', $data);
  }

  public function login(){
    $post = $this->input->post();
    $validation = $this->form_validation;

    if($post['type'] == '1'){
      $pasien = $this->pasien;
      $validation->set_rules($pasien->rulesLogin());
      if($validation->run()){
        $data = $pasien->login();

        if(!empty($data)){
          $array = array(
            "id" => $data->id,
            "nik" => $data->nik,
            "nama" => $data->nama,
            "level" => 1,
            "login" => true
          );

          $this->session->set_userdata($array);
          flashData('success', 'Selamat datang pasien '.$data->nama.'.');

          return redirect('dashboard');
        }else{
          flashData('danger', 'NIK dan password tidak valid!');

          return redirect('/');
        }
      }
    }else{
      $dokter = $this->dokter;
      $validation->set_rules($dokter->rulesLogin());
      if($validation->run()){
        $data = $dokter->login();

        if(!empty($data)){
          $array = array(
            "id" => $data->id,
            "nip" => $data->nip,
            "nama" => $data->nama,
            "spesialisId" => $data->spesialisId,
            "level" => 2,
            "login" => true
          );

          $this->session->set_userdata($array);
          flashData('success', 'Selamat datang dokter '.$data->nama.', selamat bekerja.');

          return redirect('dashboard');
        }else{
          flashData('danger', 'NIP dan password tidak valid!');

          return redirect('/');
        }
      }
    }
    
  }

  public function logoutPasien(){
    cekLoginUser();
    $array = array('id', 'nik', 'nama', 'level', 'login');
    $this->session->unset_userdata($array);
    return redirect('/');
  }

  public function logoutDokter(){
    cekLoginUser();
    $array = array('id', 'nip', 'nama', 'spesialisId', 'level', 'login');
    $this->session->unset_userdata($array);
    return redirect('/');
  }
}
