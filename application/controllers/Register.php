<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('Pasien_model', 'pasien');
  }

	public function index(){
    $this->load->view('register');
  }

  public function store(){
    $pasien = $this->pasien;
    $validation = $this->form_validation;
    $validation->set_rules($pasien->rules());
    
    if($validation->run()){
      $pasien->store();
      flashData('success', 'Pasien berhasil terdaftar, silahkan lakukan login.');

      return redirect('/');
    }

    flashData('danger', 'Register gagal!');
    return $this->index();
  }
}
