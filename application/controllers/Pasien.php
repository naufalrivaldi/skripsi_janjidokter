<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginUser();

    $this->load->model('Pasien_model', 'pasien');
  }

	public function view(){
    $data['title'] = 'Pasien';
    $data['no'] = 1;
    $data['pasien'] = $this->pasien->find($this->session->userdata('id'));
    
		$this->load->view('pasien/view', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Pasien';

    if(empty($id)){
      $data['pasien'] = (object)[
        'id' => '',
        'nama' => '',
        'tmptLahir' => '',
        'tglLahir' => '',
        'jk' => '',
        'alamat' => '',
        'noTelp' => ''
      ];
    }else{
      $data['pasien'] = $this->pasien->find($id);
    }
    
    $this->load->view('pasien/form', $data);
  }

  public function update(){
    $pasien = $this->pasien;
    $validation = $this->form_validation;
    $validation->set_rules($pasien->rules());
    
    if($validation->run()){
      $pasien->update();
      flashData('success', 'Update data berhasil.');

      return redirect('pasien/view');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function loadPasienForm(){
    $json = [];
    $key = '';

    if(!empty($this->input->post('q'))){
      $key = $this->input->post('q');
    }

    $data = $this->db->select('id, nama')->like('nama', $key)->get('pasien')->result();
    echo json_encode($data);
  }

  public function repassword(){
    $pasien = $this->pasien;
    $validation = $this->form_validation;
    $validation->set_rules($pasien->rulesRePassword());
    if($validation->run()){
      if($pasien->checkPassword()){
        $pasien->rePassword();
        flashData('success', 'Silahkan masuk kembali.');

        return redirect('auth/logoutpasien');
      }
      
      flashData('warning', 'Password lama tidak sama!');
      return redirect('dashboard');
    }

    flashData('danger', 'Data tidak valid!');
    return redirect('dashboard');
  }
}
