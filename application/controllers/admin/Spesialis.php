<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spesialis extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginAdmin();

    $this->load->model('Spesialis_model', 'spesialis');
  }

	public function index(){
    $data['title'] = 'Spesialis';
    $data['no'] = 1;
    $data['spesialis'] = $this->spesialis->showAll();
    
		$this->load->view('admin/spesialis/index', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Spesialis';

    if(empty($id)){
      $data['spesialis'] = (object)[
        'nama' => ''
      ];
    }else{
      $data['spesialis'] = $this->spesialis->find($id);
    }
    
    $this->load->view('admin/spesialis/form', $data);
  }

  public function store(){
    $spesialis = $this->spesialis;
    $validation = $this->form_validation;
    $validation->set_rules($spesialis->rules());
    
    if($validation->run()){
      $spesialis->store();
      flashData('success', 'Simpan data berhasil.');

      return redirect('admin/spesialis');
    }

    flashData('danger', 'Simpan data gagal!');
    return $this->form();
  }

  public function update(){
    $spesialis = $this->spesialis;
    $validation = $this->form_validation;
    $validation->set_rules($spesialis->rules());
    
    if($validation->run()){
      $spesialis->update();
      flashData('success', 'Update data berhasil.');

      return redirect('admin/spesialis');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function destroy(){
    $this->spesialis->destroy();
  }

  public function loadSpesialisForm(){
    $json = [];
    $key = '';

    if(!empty($this->input->post('q'))){
      $key = $this->input->post('q');
    }

    $data = $this->db->select('id, nama')->like('nama', $key)->get('spesialis')->result();
    echo json_encode($data);
  }
}
