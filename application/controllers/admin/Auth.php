<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('User_model', 'user');
    $this->load->model('Jadwal_model', 'jadwal');
  }

	public function index(){
		$this->load->view('admin/login');
  }

  public function antrean(){
    if(!empty($this->jadwal->nomerAntrian())){
      $nomer = $this->jadwal->nomerAntrian();
      $data['nomer'] = $nomer->nomer;
    }else{
      $data['nomer'] = '-';
    }

    $this->load->view('admin/antrean', $data);
  }

  public function login(){
    $user = $this->user;
    $validation = $this->form_validation;

    $validation->set_rules($user->rulesLogin());
    if($validation->run()){
      $data = $user->login();

      if(!empty($data)){
        $array = array(
          "id" => $data->id,
          "username" => $data->username,
          "nama" => $data->nama,
          "level" => 3,
          "login" => true
        );

        $this->session->set_userdata($array);
        flashData('success', 'Selamat datang '.$data->nama.', selamat bekerja');

        return redirect('admin/dashboard');
      }else{
        flashData('danger', 'Username dan password tidak valid!');

        return redirect('admin/');
      }
    }
  }

  public function logout(){
    cekLoginAdmin();
    $array = array('id', 'username', 'nama', 'level', 'login');
    $this->session->unset_userdata($array);
    return redirect('admin/');
  }
}
