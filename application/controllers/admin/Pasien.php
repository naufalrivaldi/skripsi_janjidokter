<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginAdmin();

    $this->load->model('Pasien_model', 'pasien');
  }

	public function index(){
    $data['title'] = 'Pasien';
    $data['no'] = 1;
    $data['pasien'] = $this->pasien->showAll();
    
		$this->load->view('admin/pasien/index', $data);
  }

  public function form($id = null){
    $data['title'] = 'Form Pasien';

    if(empty($id)){
      $data['pasien'] = (object)[
        'id' => '',
        'nik' => '',
        'nama' => '',
        'tmptLahir' => '',
        'tglLahir' => '',
        'jk' => '',
        'alamat' => '',
        'noTelp' => ''
      ];
    }else{
      $data['pasien'] = $this->pasien->find($id);
    }
    
    $this->load->view('admin/pasien/form', $data);
  }

  public function store(){
    $pasien = $this->pasien;
    $validation = $this->form_validation;
    $validation->set_rules($pasien->rules());
    
    if($validation->run()){
      $pasien->store();
      flashData('success', 'Simpan data berhasil.');

      return redirect('admin/pasien');
    }

    flashData('danger', 'Simpan data gagal!');
    return $this->form();
  }

  public function update(){
    $pasien = $this->pasien;
    $validation = $this->form_validation;
    $validation->set_rules($pasien->rules());
    
    if($validation->run()){
      $pasien->update();
      flashData('success', 'Update data berhasil.');

      return redirect('admin/pasien');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function reset(){
    $this->pasien->resetPassword();
  }

  public function destroy(){
    $this->pasien->destroy();
  }

  public function loadPasienForm(){
    $json = [];
    $key = '';

    if(!empty($this->input->post('q'))){
      $key = $this->input->post('q');
    }

    $data = $this->db->select('id, nama')->like('nama', $key)->get('pasien')->result();
    echo json_encode($data);
  }
}
