<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginAdmin();

    $this->load->model('Jadwal_model', 'jadwal');
    $this->load->model('Dokter_model', 'dokter');
    $this->load->model('Janji_model', 'janji');

    $this->load->library('pdf');
  }

	public function index(){
    $data['title'] = 'Jadwal';
    $data['no'] = 1;
    $data['jadwal'] = $this->jadwal->showAll();
    
		$this->load->view('admin/jadwal/index', $data);
  }

  public function form($janjiId = null, $id = null){
    $data['title'] = 'Form Jadwal';
    $data['janji'] = $this->janji->find($janjiId);

    if(empty($id)){
      $data['jadwal'] = (object)[
        'id' => '',
        'tgl' => date('Y-m-d'),
        'janjiId' => $janjiId,
        'dokterId' => ''
      ];
    }else{
      $data['jadwal'] = $this->jadwal->find($id);
    }
    
    $this->load->view('admin/jadwal/form', $data);
  }

  public function printAntrean($id){
    $data['jadwal'] = $this->jadwal->find($id);
    
    $this->pdf->setPaper('A8', 'potrait');
    $this->pdf->filename = 'nomer-antrian.pdf';
    $this->pdf->load_view('admin/jadwal/print', $data);
  }

  public function store(){
    $jadwal = $this->jadwal;
    $janji = $this->janji;
    $validation = $this->form_validation;
    $validation->set_rules($jadwal->rules());
    
    if($validation->run()){
      $jadwal->store();
      $janji->setStatus();
      flashData('success', 'Jadwal berhasil dibuat.');

      return redirect('admin/jadwal');
    }

    flashData('danger', 'Jadwal gagal dibuat!');
    return $this->form();
  }

  public function update(){
    $jadwal = $this->jadwal;
    $validation = $this->form_validation;
    $validation->set_rules($jadwal->rules());
    
    if($validation->run()){
      $jadwal->update();
      flashData('success', 'Update data berhasil.');

      return redirect('admin/jadwal');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function destroy(){
    $this->janji->setStatusDelete();
    $this->jadwal->destroy();
  }

  public function view($id){
    $data['title'] = 'Detail Jadwal';
    $data['jadwal'] = $this->jadwal->find($id);

    $this->load->view('admin/jadwal/view', $data);
  }
}
