<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekamMedis extends CI_Controller {

	public function __construct(){
    parent::__construct();

    cekLoginUser();

    $this->load->model('Pasien_model', 'pasien');
    $this->load->model('Jadwal_model', 'jadwal');
    $this->load->model('Dokter_model', 'dokter');
    $this->load->model('RekamMedis_model', 'rekamMedis');

    $this->load->library('pdf');
  }

	public function index(){
    $data['title'] = 'Rekam Medis';
    $data['no'] = 1;
    if($this->session->userdata('level') == 1){
      $data['rekamMedis'] = $this->rekamMedis->showAllPasien($this->session->userdata('id'));
    }else{
      $data['rekamMedis'] = $this->rekamMedis->showAll();
    }
    
		$this->load->view('rekam_medis/index', $data);
  }

  public function form($jadwalId = null, $id = null){
    $data['title'] = 'Form Rekam Medis';
    $data['jadwal'] = $this->jadwal->find($jadwalId);

    if(empty($id)){
      $data['rekamMedis'] = (object)[
        'id' => '',
        'jadwalId' => $jadwalId,
        'catatan' => ''
      ];
    }else{
      $data['rekamMedis'] = $this->rekamMedis->find($id);
    }
    
    $this->load->view('rekam_medis/form', $data);
  }

  public function detail($pasienId){
    $data['title'] = 'Detail Rekam Medis';
    $data['no'] = 1;
    $data['pasien'] = $this->pasien->find($pasienId);
    $data['rekamMedis'] = $this->rekamMedis->showAllDetail($pasienId);

    $this->load->view('rekam_medis/detail', $data);
  }

  public function print($pasienId){
    $data['no'] = 1;
    $pasien = $this->pasien->find($pasienId);
    $data['pasien'] = $pasien;
    $data['rekamMedis'] = $this->rekamMedis->showAllDetail($pasienId);

    $this->load->view('rekam_medis/print', $data);
    $this->pdf->setPaper('A4', 'potrait');
    $this->pdf->filename = 'rekam-medis-'.$pasien->nama.'.pdf';
    $this->pdf->load_view('rekam_medis/print', $data);
  }

  public function store(){
    $jadwal = $this->jadwal;
    $rekamMedis = $this->rekamMedis;
    $validation = $this->form_validation;
    $validation->set_rules($rekamMedis->rules());
    
    if($validation->run()){
      $rekamMedis->store();
      $jadwal->setStatus();
      flashData('success', 'Rekam medis berhasil dibuat.');

      return redirect('rekammedis');
    }

    flashData('danger', 'Rekam medis gagal dibuat!');
    return $this->form();
  }

  public function update(){
    $rekamMedis = $this->rekamMedis;
    $validation = $this->form_validation;
    $validation->set_rules($rekamMedis->rules());
    
    if($validation->run()){
      $rekamMedis->update();
      flashData('success', 'Update data berhasil.');

      return redirect('rekammedis');
    }

    flashData('danger', 'Update data gagal!');
    return $this->form();
  }

  public function destroy(){
    $this->jadwal->setStatusDelete();
    $this->rekamMedis->destroy();
  }

  public function view(){
    $get = $this->input->get();
    $id = $get['id'];

    $data['rekamMedis'] = $this->rekamMedis->find($id);
    $this->load->view('rekam_medis/view', $data);
  }
}
