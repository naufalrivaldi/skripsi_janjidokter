<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_model extends CI_Model {

  public $nomer;
  public $tgl;
  public $status;
  public $dokterId;
  public $janjiId;

	public function rules(){
    return [
      [
        'field' => 'tgl',
        'label' => 'tgl',
        'rules' => 'required'
      ],
      [
        'field' => 'dokterId',
        'label' => 'dokterId',
        'rules' => 'required'
      ],
      [
        'field' => 'janjiId',
        'label' => 'janjiId',
        'rules' => 'required|numeric'
      ]
    ];
  }

  public function showAll(){
    $tgl = '';
    $status = '';
    if($_GET){
      $tgl = $_GET['tgl'];
      $status = $_GET['status'];
    }

    return $this->db->select('j.id, j.nomer, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.nama as namaPasien, s.nama as namaSpesialis, jan.keterangan')
                      ->from('jadwal j')
                      ->join('dokter d', 'd.id = j.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = j.janjiId')
                      ->join('pasien p', 'p.id = jan.pasienId')
                      ->order_by('j.tgl', 'desc')
                      ->order_by('j.nomer', 'desc')
                      ->like('j.tgl', $tgl)
                      ->like('j.status', $status)
                      ->get()
                      ->result();
  }

  public function showAllData(){
    $tgl = '';
    $status = '';
    if($_GET){
      $tgl = $_GET['tgl'];
      $status = $_GET['status'];
    }

    if($this->session->userdata('level') == '2'){
      return $this->db->select('j.id, j.nomer, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.nama as namaPasien, s.nama as namaSpesialis, jan.keterangan')
                        ->from('jadwal j')
                        ->join('dokter d', 'd.id = j.dokterId')
                        ->join('spesialis s', 's.id = d.spesialisId')
                        ->join('janji jan', 'jan.id = j.janjiId')
                        ->join('pasien p', 'p.id = jan.pasienId')
                        ->order_by('j.tgl', 'desc')
                        ->order_by('j.nomer', 'desc')
                        ->where('j.dokterId', $this->session->userdata('id'))
                        ->like('j.tgl', $tgl)
                        ->like('j.status', $status)
                        ->get()
                        ->result();
    }else{
      return $this->db->select('j.id, j.nomer, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.id as pasienId, p.nama as namaPasien, s.nama as namaSpesialis, jan.keterangan')
                      ->from('jadwal j')
                      ->join('dokter d', 'd.id = j.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = j.janjiId')
                      ->join('pasien p', 'p.id = jan.pasienId')
                      ->order_by('j.tgl', 'desc')
                      ->order_by('j.nomer', 'desc')
                      ->where('pasienId', $this->session->userdata('id'))
                      ->like('j.tgl', $tgl)
                      ->like('j.status', $status)
                      ->get()
                      ->result();
    }
  }

  public function find($id){
    return $this->db->select('j.id, j.nomer, j.tgl, j.status, j.dokterId, j.janjiId, d.nama as namaDokter, p.id as pasienId, p.nama as namaPasien, s.nama as namaSpesialis, jan.tgl as tglJanji, jan.keterangan')
                      ->from('jadwal j')
                      ->join('dokter d', 'd.id = j.dokterId')
                      ->join('spesialis s', 's.id = d.spesialisId')
                      ->join('janji jan', 'jan.id = j.janjiId')
                      ->join('pasien p', 'p.id = jan.pasienId')
                      ->order_by('j.tgl', 'desc')
                      ->order_by('j.nomer', 'desc')
                      ->where('j.id', $id)
                      ->get()
                      ->row();
  }

  public function nomerAntrian(){
    return $this->db->select('nomer')->where('tgl', date('Y-m-d'))->where('status', '1')->order_by('nomer', 'asc')->get('jadwal')->row();
  }

  public function nomerAntrianPasien($id){
    return $this->db->select('jad.id, jad.nomer, jad.tgl, jad.status, jan.pasienId')
                      ->from('jadwal jad')
                      ->join('janji jan', 'jan.id = jad.janjiId')
                      ->where('jad.status', '1')
                      ->where('jan.pasienId', $id)
                      ->where('jan.tgl >=', date('Y-m-d'))
                      ->order_by('jad.nomer', 'asc')
                      ->get()
                      ->row();
  }

  public function store(){
    $post = $this->input->post();
    $this->nomer = $this->nomer($post['tgl']);
    $this->tgl = $post['tgl'];
    $this->status = '1';
    $this->dokterId = $post['dokterId'];
    $this->janjiId = $post['janjiId'];

    return $this->db->insert('jadwal', $this);
  }

  public function update(){
    $post = $this->input->post();
    $nomer = $post['nomer'];
    if($post['tglOld'] != $post['tgl']){
      $nomer = $this->nomer($post['tgl']);
    }

    $data = array(
      'nomer' => $nomer,
      'tgl' => $post['tgl'],
      'dokterId' => $post['dokterId'],
      'janjiId' => $post['janjiId']
    );

    return $this->db->where('id', $post['id'])->update('jadwal', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('jadwal');
  }

  // nomer antrean
  public function nomer($tgl){
    $no = 'A01';
    $jadwal = $this->db->where('tgl', $tgl)->order_by('id', 'desc')->get('jadwal')->row();
    if(empty($jadwal)){
      return $no;
    }else{
      $array = explode('A', $jadwal->nomer);
      (float)$array[1] += 1;
      if(strlen($array[1]) == '1'){
        $array[1] = '0'.$array[1];
      }

      return 'A'.$array[1];
    }
  }

  // set status
  public function setStatus(){
    $post = $this->input->post();
    $array = array(
      'status' => '0'
    );
    
    return $this->db->where('id', $post['jadwalId'])->update('jadwal', $array);
  }

  public function setStatusDelete(){
    $post = $this->input->post();
    $data = $this->db->where('id', $post['id'])->get('rekam_medis')->row();
    $array = array(
      'status' => '1'
    );
    
    return $this->db->where('id', $data->jadwalId)->update('jadwal', $array);
  }
}
