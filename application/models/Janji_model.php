<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Janji_model extends CI_Model {

  public $tgl;
  public $keterangan;
  public $status;
  public $pasienId;
  public $spesialisId;

	public function rules(){
    return [
      [
        'field' => 'tgl',
        'label' => 'tgl',
        'rules' => 'required'
      ],
      [
        'field' => 'keterangan',
        'label' => 'keterangan',
        'rules' => 'required'
      ],
      [
        'field' => 'pasienId',
        'label' => 'pasienId',
        'rules' => 'required|numeric'
      ],
      [
        'field' => 'spesialisId',
        'label' => 'spesialisId',
        'rules' => 'required|numeric'
      ]
    ];
  }

  public function showAll(){
    return $this->db->select('j.id, j.tgl, j.keterangan, j.status, j.pasienId, j.spesialisId, p.nama as namaPasien, s.nama as namaSpesialis')
                      ->from('janji j')
                      ->join('pasien p', 'p.id = j.pasienId')
                      ->join('spesialis s', 's.id = j.spesialisId')
                      ->order_by('j.tgl', 'desc')
                      ->get()
                      ->result();
  }

  public function showAllData(){
    if($this->session->userdata('level') == '1'){
      return $this->db->select('j.id, j.tgl, j.keterangan, j.status, j.pasienId, j.spesialisId, p.nama as namaPasien, s.nama as namaSpesialis')
                      ->from('janji j')
                      ->join('pasien p', 'p.id = j.pasienId')
                      ->join('spesialis s', 's.id = j.spesialisId')
                      ->order_by('j.tgl', 'desc')
                      ->where('j.pasienId', $this->session->userdata('id'))
                      ->get()
                      ->result();
    }else{
      return $this->db->select('j.id, j.tgl, j.keterangan, j.status, j.pasienId, j.spesialisId, p.nama as namaPasien, s.nama as namaSpesialis')
                      ->from('janji j')
                      ->join('pasien p', 'p.id = j.pasienId')
                      ->join('spesialis s', 's.id = j.spesialisId')
                      ->order_by('j.tgl', 'desc')
                      ->where('j.spesialisId', $this->session->userdata('spesialisId'))
                      ->get()
                      ->result();
    }
    
  }

  public function find($id){
    return $this->db->select('j.id, j.tgl, j.keterangan, j.pasienId, j.spesialisId, p.nama as namaPasien, s.nama as namaSpesialis')
                    ->from('janji j')
                    ->join('pasien p', 'p.id = j.pasienId')
                    ->join('spesialis s', 's.id = j.spesialisId')
                    ->where('j.id', $id)
                    ->get()->row();
  }

  public function store(){
    $post = $this->input->post();
    $this->tgl = $post['tgl'];
    $this->keterangan = $post['keterangan'];
    $this->status = '1';
    $this->pasienId = $post['pasienId'];
    $this->spesialisId = $post['spesialisId'];

    return $this->db->insert('janji', $this);
  }

  public function update(){
    $post = $this->input->post();
    $data = array(
      'tgl' => $post['tgl'],
      'keterangan' => $post['keterangan'],
      'pasienId' => $post['pasienId'],
      'spesialisId' => $post['spesialisId']
    );

    return $this->db->where('id', $post['id'])->update('janji', $data);
  }

  public function destroy(){
    $post = $this->input->post();
    return $this->db->where('id', $post['id'])->delete('janji');
  }

  // set status
  public function setStatus(){
    $post = $this->input->post();
    $array = array(
      'status' => '0'
    );
    
    return $this->db->where('id', $post['janjiId'])->update('janji', $array);
  }

  public function setStatusDelete(){
    $post = $this->input->post();
    $data = $this->db->where('id', $post['id'])->get('jadwal')->row();
    $array = array(
      'status' => '1'
    );
    
    return $this->db->where('id', $data->janjiId)->update('janji', $array);
  }
}
