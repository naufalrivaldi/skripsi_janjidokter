<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <a href="<?= site_url('rekammedis') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-5">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-calendar"></i> Data Jadwal</h5>
            </div>
            <div class="card-body">
              <h6>Tanggal</h6>
              <p><?= setDate($jadwal->tgl) ?></p>
              <h6>Nama Pasien</h6>
              <p><?= $jadwal->namaPasien ?></p>
              <h6>Nama Dokter</h6>
              <p><?= $jadwal->namaDokter ?></p>
              <h6>Spesialis Terkait</h6>
              <p><?= $jadwal->namaSpesialis ?></p>
              <h6>Keterangan</h6>
              <p><?= $jadwal->keterangan ?></p>
            </div>
          </div>
        </div>

        <div class="col-md-7">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-hospital-o"></i> Rekam Medis</h5>
            </div>
            <div class="card-body">
              <form action="<?= (empty($rekammedis->id))? site_url('rekammedis/store') : site_url('rekammedis/update') ?>" method="POST">
                <?php if(!empty($rekamMedis->id)): ?>
                <input type="hidden" name="id" value="<?= $rekamMedis->id ?>">
                <?php endif ?>
                
                <input type="hidden" name="jadwalId" value="<?= $jadwal->id ?>">
                <input type="hidden" name="pasienId" value="<?= $jadwal->pasienId ?>">
                <div class="form-group">
                  <label for="catatan">Catatan</label>
                  <textarea name="catatan" id="catatan" class="form-control" rows="20"><?= $rekamMedis->catatan ?></textarea>
                  
                  <small class="text-danger"><?= form_error('catatan') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Buat Rekam Medis</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    
  </script>
  <!-- js -->
</body>

</html>
