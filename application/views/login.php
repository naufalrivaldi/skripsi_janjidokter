<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta name="description" content="Janji Dokter Login">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="apple-icon.png">
  <link rel="shortcut icon" href="favicon.ico">


  <link rel="stylesheet" href="<?= base_url('vendors/bootstrap/dist/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/font-awesome/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/themify-icons/css/themify-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/flag-icon-css/css/flag-icon.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/selectFX/css/cs-skin-elastic.css') ?>">

  <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/app.css') ?>">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body class="bg-dark">
  <div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container-login">
      <div class="">
        <div class="login-logo">
          <a href="">
            <h3>SISTEM JANJI DOKTER</h3>
          </a>
        </div>
        <div class="login-form">
          <?php $this->load->view('layout/alert') ?>
          <div class="row">
            <div class="col-md-6 antrian mb-3">
              <h3>Nomer Antrian :</h3>
              <p><?= $nomer ?></p>
              <small class="text-muted">Antrian tanggal <?= setDate(date('Y-m-d')) ?></small>
            </div>
            <div class="col-md-6">
              <h3>Login</h3>
              <hr>
              <form action="<?= site_url('auth/login') ?>" method="POST">
                <div class="form-group">
                  <label>Masuk sebagai?</label>
                  <select name="type" id="type" class="form-control">
                    <option value="1">Pasien</option>
                    <option value="2">Dokter</option>
                  </select>

                  <small class="form-text text-danger"><?= form_error('username') ?></small>
                </div>
                <div class="form-group form-custom">
                  <label>NIK</label>
                  <input type="text" name="nik" class="form-control" required>

                  <small class="form-text text-danger"><?= form_error('nik') ?></small>
                </div>
                
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" class="form-control" required>

                  <small class="form-text text-danger"><?= form_error('password') ?></small>
                </div>
                          
                <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30"><i class="fa fa-user"></i> Sign in</button>
              </form>
                <a href="<?= site_url('/register') ?>" class="btn btn-info btn-flat m-b-30 m-t-30 mt-2 register"><i class="fa fa-user-md"></i> Daftar Pasien</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Javascript -->
  <script src="<?= base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
  <script src="<?= base_url('vendors/popper.js/dist/umd/popper.min.js') ?>"></script>
  <script src="<?= base_url('vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
  
  <script>
    $(document).ready(function(){
      $('#type').on('change', function(){
        let type = $(this).val();
        let pasien = `
          <label>NIK</label>
          <input type="text" name="nik" class="form-control" required>

          <small class="form-text text-danger"><?= form_error('nik') ?></small>
        `;

        let dokter = `
          <label>NIP</label>
          <input type="text" name="nip" class="form-control" required>

          <small class="form-text text-danger"><?= form_error('nip') ?></small>
        `;
        if(type == '1'){
          $('.form-custom').empty();
          $('.form-custom').append(pasien);
          $('.register').removeClass('hidden');
        }else{
          $('.form-custom').empty();
          $('.form-custom').append(dokter);
          $('.register').addClass('hidden');
        }
      });
    });
  </script>
</body>

</html>
