<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('dokter/view') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
        </div>
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <form action="<?= (empty($dokter->id))? site_url('dokter/store') : site_url('dokter/update') ?>" method="POST">
                <?php if(!empty($dokter->id)): ?>
                <input type="hidden" name="id" value="<?= $dokter->id ?>">
                <?php endif ?>
                
                <div class="form-group">
                  <label for="nip">NIP</label>
                  <input type="number" name="nip" class="form-control" id="nip" value="<?= $dokter->nip ?>">

                  <small class="text-danger"><?= form_error('nip') ?></small>
                </div>

                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" name="nama" class="form-control" id="nama" value="<?= $dokter->nama ?>">

                  <small class="text-danger"><?= form_error('nama') ?></small>
                </div>

                <div class="form-group">
                  <label for="jk">Jenis Kelamin</label>
                  <select name="jk" id="jk" class="form-control">
                    <option value="">Pilih</option>
                    <option value="L" <?= ($dokter->jk == 'L')?'selected':'' ?>>Laki - laki</option>
                    <option value="P" <?= ($dokter->jk == 'P')?'selected':'' ?>>Perempuan</option>
                  </select>

                  <small class="text-danger"><?= form_error('jk') ?></small>
                </div>

                <div class="form-group">
                  <label for="noTelp">Nomer Telepon</label>
                  <input type="text" name="noTelp" class="form-control" id="noTelp" value="<?= $dokter->noTelp ?>">

                  <small class="text-danger"><?= form_error('noTelp') ?></small>
                </div>

                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea name="alamat" id="alamat" rows="5" class="form-control"><?= $dokter->alamat ?></textarea>

                  <small class="text-danger"><?= form_error('alamat') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <!-- js -->
</body>

</html>
