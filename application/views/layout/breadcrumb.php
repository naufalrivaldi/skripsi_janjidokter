<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1><?= ucwords($title) ?></h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
        <?php if($this->session->userdata('level') == '3'): ?>
          <?php for($i = 2; $i <= count($this->uri->segment_array()); $i++){ ?>
            <li class="breadcrumb-item"><?= ucwords($this->uri->segment($i)) ?></li>
          <?php } ?>
        <?php else: ?>
          <?php for($i = 1; $i <= count($this->uri->segment_array()); $i++){ ?>
            <li class="breadcrumb-item"><?= ucwords($this->uri->segment($i)) ?></li>
          <?php } ?>
        <?php endif ?>
        </ol>
      </div>
    </div>
  </div>
</div>