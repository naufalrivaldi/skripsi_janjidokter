<!-- modal -->
<div class="modal fade" id="rePassword" tabindex="-1" role="dialog" aria-labelledby="rePasswordLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="rePasswordLabel">
          <i class="fa fa-key text-success"></i> Ubah Password
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?= site_url(rePasswordLink()) ?>" method="POST">
      <div class="modal-body">
        <div class="form-group">
          <label for="oldPassword">Password lama</label>
          <input type="password" name="oldPassword" class="form-control" id="oldPassword" required>

          <small class="text-danger"><?= form_error('oldPassword') ?></small>
        </div>

        <div class="form-group">
          <label for="newPassword">Password Baru</label>
          <input type="password" name="newPassword" class="form-control" id="newPassword" required>

          <small class="text-danger"><?= form_error('newPassword') ?></small>
        </div>

        <div class="form-group">
          <label for="confirmPassword">Konfirmasi Password</label>
          <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" required>

          <small class="text-danger"><?= form_error('confirmPassword') ?></small>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- modal -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="<?= base_url('vendors/popper.js/dist/umd/popper.min.js') ?>"></script>
<script src="<?= base_url('vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

<script src="<?= base_url('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= base_url('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?= base_url('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') ?>"></script>
<script src="<?= base_url('vendors/jszip/dist/jszip.min.js') ?>"></script>
<script src="<?= base_url('vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('vendors/pdfmake/build/vfs_fonts.js') ?>"></script>
<script src="<?= base_url('vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
<script src="<?= base_url('vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
<script src="<?= base_url('vendors/datatables.net-buttons/js/buttons.colVis.min.js') ?>"></script>
<script src="<?= base_url('assets/js/init-scripts/data-table/datatables-init.js') ?>"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script src="https://cdn.tiny.cloud/1/8umgjhgub5p9ybjnnc9zo5xwvo264tfnvficzvbynegdl1c4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  $(document).ready(function(){
    $('#menuToggle').on('click', function(event) {
      $('body').toggleClass('open');
    });

    tinymce.init({
      selector: '#catatan',
    });

    $('.form-filter').on('change', function(){
      $('.form-filter').submit();
    });
  })
</script>