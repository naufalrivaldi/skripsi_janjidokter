<header id="header" class="header">
  <div class="header-menu">
    <div class="col-sm-12">
      <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
      <div class="user-area dropdown float-right">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="navbar-text mr-2">
            <?= $this->session->userdata('nama') ?>
          </span>
          <img class="user-avatar rounded-circle" src="<?= base_url('images/admin.jpg') ?>" alt="User Avatar">
        </a>

        <div class="user-menu dropdown-menu">
          <?php if($this->session->userdata('level') == '1'): ?>
            <h6 class="mt-2">Pasien</h6><hr>
            <a class="nav-link" href="<?= site_url('pasien/view') ?>"><i class="fa fa-user"></i> User</a>
          <?php endif ?>
          <?php if($this->session->userdata('level') == '2'): ?>
            <h6 class="mt-2">Dokter</h6><hr>
            <a class="nav-link" href="<?= site_url('dokter/view') ?>"><i class="fa fa-user"></i> User</a>
          <?php endif ?>
          <a class="nav-link" href="#" data-toggle="modal" data-target="#rePassword"><i class="fa fa-key"></i> Ubah Password</a>

          <?php if($this->session->level == '3'): ?>
          <a class="nav-link" href="<?= site_url('admin/logout') ?>"><i class="fa fa-power-off"></i> Logout</a>
          <?php endif ?>

          <?php if($this->session->level == '1'): ?>
          <a class="nav-link" href="<?= site_url('auth/logoutPasien') ?>"><i class="fa fa-power-off"></i> Logout</a>
          <?php endif ?>

          <?php if($this->session->level == '2'): ?>
          <a class="nav-link" href="<?= site_url('auth/logoutDokter') ?>"><i class="fa fa-power-off"></i> Logout</a>
          <?php endif ?>
        </div>
      </div>
    </div>
  </div>
</header><!-- /header -->