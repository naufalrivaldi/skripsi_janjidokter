<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta name="description" content="Janji Dokter Login">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="apple-icon.png">
  <link rel="shortcut icon" href="favicon.ico">


  <link rel="stylesheet" href="<?= base_url('vendors/bootstrap/dist/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/font-awesome/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/themify-icons/css/themify-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/flag-icon-css/css/flag-icon.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/selectFX/css/cs-skin-elastic.css') ?>">

  <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/app.css') ?>">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body class="bg-dark">
  <div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
      <div class="login-content">
        <div class="login-logo">
          <a href="">
            <h3>SISTEM JANJI DOKTER</h3>
          </a>
        </div>
          <div class="login-form">
            <h3>Register Pasien</h3>
            <hr>
            <?php $this->load->view('layout/alert') ?>
            <form method="POST" action="<?= site_url('register/store') ?>">
              <div class="form-group">
                <label for="nik">NIK</label>
                <input type="number" name="nik" class="form-control" id="nik">

                <small class="text-danger"><?= form_error('nik') ?></small>
              </div>

              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" name="nama" class="form-control" id="nama">

                <small class="text-danger"><?= form_error('nama') ?></small>
              </div>

              <div class="form-group">
                <label>TTL</label>
                <div class="row">
                  <div class="col-md-6">
                    <input type="text" name="tmptLahir" class="form-control" id="tmptLahir" placeholder="Tempat">

                    <small class="text-danger"><?= form_error('tmptLahir') ?></small>
                  </div>
                  <div class="col-md-6">
                    <input type="date" name="tglLahir" class="form-control" id="tglLahir">

                    <small class="text-danger"><?= form_error('tmptLahir') ?></small>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="jk">Jenis Kelamin</label>
                <select name="jk" id="jk" class="form-control">
                  <option value="">Pilih</option>
                  <option value="L">Laki - laki</option>
                  <option value="P">Perempuan</option>
                </select>

                <small class="text-danger"><?= form_error('jk') ?></small>
              </div>

              <div class="form-group">
                <label for="noTelp">Nomer Telepon</label>
                <input type="text" name="noTelp" class="form-control" id="noTelp">

                <small class="text-danger"><?= form_error('noTelp') ?></small>
              </div>

              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea name="alamat" id="alamat" rows="5" class="form-control"></textarea>

                <small class="text-danger"><?= form_error('alamat') ?></small>
              </div>

              <div class="alert alert-info" role="alert">
                *Password akan diset default '12345'.
              </div>

              <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Register</button>
              <div class="register-link m-t-15 text-center">
                <p>Sudah punya akun pasien? <a href="<?= site_url('/') ?>"> Masuk</a></p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  <!-- Javascript -->
  <script src="<?= base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
  <script src="<?= base_url('vendors/popper.js/dist/umd/popper.min.js') ?>"></script>
  <script src="<?= base_url('vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
  
  <script>
    $(document).ready(function(){
      $('#type').on('change', function(){
        let type = $(this).val();
        let pasien = `
          <label>NIK</label>
          <input type="text" name="nik" class="form-control" required>

          <small class="form-text text-danger"><?= form_error('nik') ?></small>
        `;

        let dokter = `
          <label>NIP</label>
          <input type="text" name="nip" class="form-control" required>

          <small class="form-text text-danger"><?= form_error('nip') ?></small>
        `;
        if(type == '1'){
          $('.form-custom').empty();
          $('.form-custom').append(pasien);
          $('.register').removeClass('hidden');
        }else{
          $('.form-custom').empty();
          $('.form-custom').append(dokter);
          $('.register').addClass('hidden');
        }
      });
    });
  </script>
</body>

</html>
