<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta name="description" content="Janji Dokter Login">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="apple-icon.png">
  <link rel="shortcut icon" href="favicon.ico">


  <link rel="stylesheet" href="<?= base_url('vendors/bootstrap/dist/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/font-awesome/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/themify-icons/css/themify-icons.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/flag-icon-css/css/flag-icon.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('vendors/selectFX/css/cs-skin-elastic.css') ?>">

  <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body class="bg-dark">
  <div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
      <div class="login-content">
        <div class="login-logo">
          <a href="index.html">
            <h3>SISTEM JANJI DOKTER</h3>
          </a>
        </div>
        <div class="login-form">
          <?php $this->load->view('layout/alert') ?>
          <form action="<?= site_url('admin/login') ?>" method="POST">
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" class="form-control" autofocus required>

              <small class="form-text text-danger"><?= form_error('username') ?></small>
            </div>
            
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" required>

              <small class="form-text text-danger"><?= form_error('password') ?></small>
            </div>
                      
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30"><i class="fa fa-user"></i> Sign in</button>
            <p class="text-center mt-3"><a href="<?= site_url('admin/antrean') ?>" target="_BLANK">Lihat Antrian</a></p>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Javascript -->
  <script src="<?= base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
  <script src="<?= base_url('vendors/popper.js/dist/umd/popper.min.js') ?>"></script>
  <script src="<?= base_url('vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
  <script src="<?= base_url('assets/js/main.js') ?>"></script>
</body>

</html>
