<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('admin/janji') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
        </div>
        <div class="card-body">
          <div class="row justify-content-center">
            <div class="col-md-6">
              <form action="<?= (empty($janji->id))? site_url('admin/janji/store') : site_url('admin/janji/update') ?>" method="POST">
                <?php if(!empty($janji->id)): ?>
                <input type="hidden" name="id" value="<?= $janji->id ?>">
                <?php endif ?>
                
                <div class="form-group">
                  <label for="tgl">Tanggal</label>
                  <input type="date" name="tgl" class="form-control" id="tgl" value="<?= $janji->tgl ?>" readonly>

                  <small class="text-danger"><?= form_error('tgl') ?></small>
                </div>

                <div class="form-group">
                  <label for="pasienId">Pasien</label>
                  <select name="pasienId" id="pasienId" class="form-control">
                    <?php if(!empty($janji->id)): ?>
                      <option value="<?= $janji->pasienId ?>" selected><?= $janji->namaPasien ?></option>
                    <?php endif ?>
                  </select>

                  <small class="text-danger"><?= form_error('pasienId') ?></small>
                </div>

                <div class="form-group">
                  <label for="spesialisId">Spesialis</label>
                  <select name="spesialisId" id="spesialisId" class="form-control">
                    <?php if(!empty($janji->id)): ?>
                      <option value="<?= $janji->spesialisId ?>" selected><?= $janji->namaSpesialis ?></option>
                    <?php endif ?>
                  </select>

                  <small class="text-danger"><?= form_error('spesialisId') ?></small>
                </div>

                <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <textarea name="keterangan" id="keterangan" rows="5" class="form-control"><?= $janji->keterangan ?></textarea>

                  <small class="text-danger"><?= form_error('keterangan') ?></small>
                </div>

                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Buat Janji</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <script>
    $('#pasienId').select2({
      placeholder: 'Cari Pasien...',
      theme: "bootstrap",
      ajax: {
        url: "<?= site_url('admin/pasien/loadPasienForm') ?>",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results: $.map(data, function(item){
              return {
                text: item.nama,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });

    $('#spesialisId').select2({
      placeholder: 'Cari Spesialis...',
      theme: "bootstrap",
      ajax: {
        url: "<?= site_url('admin/spesialis/loadSpesialisForm') ?>",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results: $.map(data, function(item){
              return {
                text: item.nama,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
  </script>
  <!-- js -->
</body>

</html>
