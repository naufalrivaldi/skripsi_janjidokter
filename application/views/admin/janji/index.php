<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="card">
        <div class="card-header">
          <a href="<?= site_url('admin/janji/form') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Tambah</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered table-custom">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nama Pasien</th>
                  <th>Spesialis</th>
                  <th width="25%">keterangan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($janji as $row): ?>
                <?php if($row->status == 1): ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= setDate($row->tgl) ?></td>
                  <td><?= $row->namaPasien ?></td>
                  <td><?= $row->namaSpesialis ?></td>
                  <td><?= $row->keterangan ?></td>
                  <td>
                    <a href="<?= site_url('admin/jadwal/form/').$row->id ?>" class="btn btn-info btn-sm"><i class="fa fa-calendar"></i> Set Jadwal</a>
                    <a href="<?= site_url('admin/janji/form/').$row->id ?>" class="btn btn-success btn-sm"><i class="fa fa-cog"></i></a>
                    <button class="btn btn-danger btn-sm delete" data-id="<?= $row->id ?>"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                <?php endif ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  
  <script>
     $(document).on('click', '.delete', function(){
      var id = $(this).data('id');
      
      Swal.fire({
        title: 'Hapus Data Janji?',
        text: "Data akan terhapus secara permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= site_url('admin/janji/destroy') ?>",
            type: 'POST',
            data: {
              id:id
            },
            success: function(data){
              location.reload();
            }
          });
        }
      })
    });

    $(document).on('click', '.reset', function(){
      var id = $(this).data('id');
      
      Swal.fire({
        title: 'Reset password janji?',
        text: "Password akan terset default 12345",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?= site_url('admin/janji/reset') ?>",
            type: 'POST',
            data: {
              id:id
            },
            success: function(data){
              location.reload();
            }
          });
        }
      })
    });
  </script>
  <!-- js -->
</body>

</html>
