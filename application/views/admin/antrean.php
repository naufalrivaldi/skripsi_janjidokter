<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Antrian</title>
    <style type="text/css">
      body{
        margin: 0;
        padding: 0;
        font-size: 5em;
      }

      .wreapper{
        width: 100%;
        height: 100vh;
      }

      .header{
        background: #3498db;
        font-size: .7em;
        color: white;
      }

      .row{
        padding: 0;
        margin: 0;
      }

      footer{
        font-size: .5em;
        text-align: left;
        position: absolute;
        bottom: 0;
        background: #3498db;
        padding: 1%;
        color: white;
        width: 100%;
      }
    </style>
  </head>
  <body>
    <div class="wreapper text-center">
      <div class="header">
        SISTEM JANJI DOKTER
      </div>
      <p>Nomer Antrian :</p>
      <h1><?= $nomer ?></h1>
      <footer>
        Tanggal : <?= date('d F Y') ?>
      </footer>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>