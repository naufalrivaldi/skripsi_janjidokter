<!doctype html>
<html class="no-js" lang="en">
<head>
  <?php $this->load->view('layout/head') ?>
</head>

<body>
  <!-- Left Panel -->
  <?php $this->load->view('layout/sidebar') ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header-->
    <?php $this->load->view('layout/navbar') ?>
    <!-- Header-->

    <!-- breadcrumb -->
    <?php $this->load->view('layout/breadcrumb') ?>
    <!-- breadcrumb -->

    <!-- content -->
    <div class="content mt-3">
      <!-- alert -->
      <?php $this->load->view('layout/alert') ?>
      <!-- alert -->
      
      <div class="row">
        <div class="col-md-4">
          <div class="card bg-success card-costume">
            <div class="card-header">
              <h4><i class="fa fa-calendar-o"></i> Nomer Antrian Berjalan</h4>
            </div>
            <div class="card-body">
              <h1><?= $nomer ?></h1>
            </div>
            <div class="card-footer">
              <p>Tanggal : <?= setDate(date('Y-m-d')) ?></p>
            </div>
          </div>
        </div>
        
        <?php if(isPasien()): ?>
        <div class="col-md-4">
          <div class="card bg-success card-costume">
            <div class="card-header">
              <h4><i class="fa fa-calendar-o"></i> Nomer Antrian Saya</h4>
            </div>
            <div class="card-body">
              <h1><?= $nomerPasien->nomer ?></h1>
              <a href="<?= site_url('jadwal/view/'.$nomerPasien->id) ?>" class="btn btn-light btn-sm <?= (empty($nomerPasien->id))?'disabled':'' ?>">Detail Antrean</a>
            </div>
            <div class="card-footer">
              <p>Tanggal : <?= (empty($nomerPasien->tgl))? '-' : setDate($nomerPasien->tgl) ?></p>
            </div>
          </div>
        </div>
        <?php endif ?>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p>Selamat datang di sistem <b>Janji Dokter</b>.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .content -->
  </div>
  <!-- Right Panel -->

  <!-- js -->
  <?php $this->load->view('layout/javascript') ?>
  <!-- js -->
</body>

</html>
