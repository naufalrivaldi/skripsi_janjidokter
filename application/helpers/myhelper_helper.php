<?php
  
  function isPasien(){
    $CI =& get_instance();
    if($CI->session->userdata('level') == '1'){
      return true;
    }

    return false;
  }

  function isDokter(){
    $CI =& get_instance();
    if($CI->session->userdata('level') == '2'){
      return true;
    }

    return false;
  }

  function cekLoginAdmin(){
    $CI =& get_instance();
    
    $login = $CI->session->userdata('loggedIn');
    $level = $CI->session->userdata('level');
    if($login != true && $level != 3){
      flashData('danger', 'Login terlebih dahulu!');
      return redirect('admin');;
    }
  }

  function cekLoginUser(){
    $CI =& get_instance();
    
    $login = $CI->session->userdata('loggedIn');
    $level = $CI->session->userdata('level');
    if($login != true && $level != 1 && $level != 2){
      flashData('danger', 'Login terlebih dahulu!');
      return redirect('/');;
    }
  }

  function flashData($type, $message){
    $CI =& get_instance();
    return $CI->session->set_flashData($type, $message);
  }

  function setDate($date){
    return date('d-m-Y', strtotime($date));
  }

  function status($val){
    $text = '';
    if($val == 1){
      $text = 'Pending';
    }else{
      $text = 'Selesai';
    }

    return $text;
  }

  function rePasswordLink(){
    $CI =& get_instance();
    $url = '';
    $level = $CI->session->userdata('level');

    switch ($level) {
      case '1':
        $url = 'pasien/repassword';
        break;

      case '2':
        $url = 'dokter/repassword';
        break;

      case '3':
        $url = 'admin/user/repassword';
        break;
      
      default:
        # code...
        break;
    }

    return $url;
  }

?>