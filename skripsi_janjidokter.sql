-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2020 at 03:56 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_janjidokter`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama`) VALUES
(1, 'noval', '8c0c29a2771c182196978f744ee769dc5b2ea090', 'Naufal Rivaldi'),
(3, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin'),
(4, 'admin2', '8cb2237d0679ca88db6464eac60da96345513964', 'admin2');

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jk` enum('L','P') NOT NULL,
  `alamat` text,
  `noTelp` varchar(16) DEFAULT NULL,
  `spesialisId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id`, `nip`, `password`, `nama`, `jk`, `alamat`, `noTelp`, `spesialisId`) VALUES
(2, '3349082', '8c0c29a2771c182196978f744ee769dc5b2ea090', 'Winy Pebriasari', 'P', 'Jl. Gunung Karang III Gg. Burung No. 19x2', '0896233960912', 2),
(3, '11234573', '8cb2237d0679ca88db6464eac60da96345513964', 'Test dokter', 'L', 'Jl. Gunung Karang III Gg. Burung No. 19x', '089623396091', 1),
(4, '33490822', '8cb2237d0679ca88db6464eac60da96345513964', 'Naufal Rivaldi', 'L', 'Jl. Gunung Karang III Gg. Burung No. 19x', '089623396091', 2);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `nomer` varchar(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `status` enum('1','0') DEFAULT NULL,
  `dokterId` int(11) DEFAULT NULL,
  `janjiId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `nomer`, `tgl`, `status`, `dokterId`, `janjiId`) VALUES
(7, 'A01', '2020-03-27', '0', 3, 3),
(8, 'A02', '2020-03-27', '1', 4, 2),
(9, 'A01', '2020-03-28', '0', 2, 5),
(10, 'A01', '2020-03-29', '0', 2, 6),
(11, 'A02', '2020-03-29', '0', 2, 7),
(12, 'A01', '2020-03-30', '1', 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `janji`
--

CREATE TABLE `janji` (
  `id` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `keterangan` text,
  `status` enum('1','0') NOT NULL,
  `pasienId` int(11) DEFAULT NULL,
  `spesialisId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `janji`
--

INSERT INTO `janji` (`id`, `tgl`, `keterangan`, `status`, `pasienId`, `spesialisId`) VALUES
(2, '2020-03-26', 'Gigi saya ompong', '0', 2, 2),
(3, '2020-03-26', 'Sakit perut', '0', 1, 1),
(5, '2020-03-27', 'Sakit gigi', '0', 2, 2),
(6, '2020-03-28', 'Saya ingin berobat', '0', 2, 2),
(7, '2020-03-28', 'Sakit gigiku', '0', 1, 2),
(8, '2020-03-30', 'Panu', '0', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id` int(11) NOT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tmptLahir` varchar(50) DEFAULT NULL,
  `tglLahir` date DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `alamat` text,
  `noTelp` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id`, `nik`, `password`, `nama`, `tmptLahir`, `tglLahir`, `jk`, `alamat`, `noTelp`) VALUES
(1, '21312312312', '8c0c29a2771c182196978f744ee769dc5b2ea090', 'Naufal Rivaldi', 'Denpasar', '1998-12-08', 'L', 'Jln. Imam Bonjol', '089623396091'),
(2, '213123213', '8cb2237d0679ca88db6464eac60da96345513964', 'Winy Pebriasari', 'Bandung', '1998-02-28', 'P', 'Jl. Babakansari', '089623396091'),
(3, '123123123', '8c0c29a2771c182196978f744ee769dc5b2ea090', 'Naufal Rivaldi', 'Denpasar', '1998-12-08', 'L', 'Jln. Gunung Karang III', '089623396091');

-- --------------------------------------------------------

--
-- Table structure for table `rekam_medis`
--

CREATE TABLE `rekam_medis` (
  `id` int(11) NOT NULL,
  `pasienId` int(11) DEFAULT NULL,
  `jadwalId` int(11) DEFAULT NULL,
  `catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekam_medis`
--

INSERT INTO `rekam_medis` (`id`, `pasienId`, `jadwalId`, `catatan`) VALUES
(1, 2, 9, '<p>Cabut gigi graham.<br />Obat yang diberikan bla bla bla</p>'),
(2, 2, 10, '<p>asdasdasdadasd<br />askdasdjad<br />asjdhjkahd</p>'),
(3, 1, 11, '<p>asdasdasda</p>\r\n<p>asdasd</p>'),
(4, 1, 7, '<p>test</p>');

-- --------------------------------------------------------

--
-- Table structure for table `spesialis`
--

CREATE TABLE `spesialis` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spesialis`
--

INSERT INTO `spesialis` (`id`, `nama`) VALUES
(1, 'Umum'),
(2, 'Gigi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD KEY `fk_spesialis_dokter` (`spesialisId`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dokter_jadwal` (`dokterId`),
  ADD KEY `fk_janji_jadwal` (`janjiId`);

--
-- Indexes for table `janji`
--
ALTER TABLE `janji`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pasien_janji` (`pasienId`),
  ADD KEY `fk_spesialis_janji` (`spesialisId`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_jadwal_rekam_medis` (`jadwalId`),
  ADD KEY `fk_pasien_rekam_medis` (`pasienId`);

--
-- Indexes for table `spesialis`
--
ALTER TABLE `spesialis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `janji`
--
ALTER TABLE `janji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `spesialis`
--
ALTER TABLE `spesialis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `fk_spesialis_dokter` FOREIGN KEY (`spesialisId`) REFERENCES `spesialis` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `fk_dokter_jadwal` FOREIGN KEY (`dokterId`) REFERENCES `dokter` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_janji_jadwal` FOREIGN KEY (`janjiId`) REFERENCES `janji` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `janji`
--
ALTER TABLE `janji`
  ADD CONSTRAINT `fk_pasien_janji` FOREIGN KEY (`pasienId`) REFERENCES `pasien` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_spesialis_janji` FOREIGN KEY (`spesialisId`) REFERENCES `spesialis` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD CONSTRAINT `fk_jadwal_rekam_medis` FOREIGN KEY (`jadwalId`) REFERENCES `jadwal` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pasien_rekam_medis` FOREIGN KEY (`pasienId`) REFERENCES `pasien` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
